## Install Package: 
Use `npm install` or `npm i` command to install all packages

## Run Server: 
Use `npm run start:dev` :+1: then click here to open Swagger: [BookShopAPI](http://localhost:3000/docs/api/)
Can open swagger server local: `http://localhost:3000/docs/api/` :100:

## API of Book:
- Get all books: `http://localhost:3000/api/books`
- Get book by id: `http://localhost:3000/api/book/id`
- Create book: `http://localhost:3000/api/books`
- Delete book: `http://localhost:3000/api/book/id`