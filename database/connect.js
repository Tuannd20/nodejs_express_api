const mongoose = require("mongoose");

const connectDatabase = async () => {
  // Connect to database
  try {
    const connectionString = `mongodb+srv://nodeapi:Nodeapi123@codeclass2api.vsp2vre.mongodb.net/BookShop?retryWrites=true&w=majority`;
    const value = await mongoose.connect(connectionString, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    if (value) console.log("Successfully connected to the database");
  } catch (error) {
    console.log("Could not connect to the database", error);
    process.exit();
  }
};

const disconnectDatabase = async () => {
  // Disconnect from database
  try {
    const value = await mongoose.connection.close();
    if (value) console.log("Disconnect DB Okay !!!");
  } catch (error) {
    console.log(
      "🚀 ~ file: connect.js ~ line 31 ~ disconnectDatabase ~ error",
      error
    );
  }
};

module.exports = {
  connectDatabase,
  disconnectDatabase,
};
