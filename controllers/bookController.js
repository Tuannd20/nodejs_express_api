const BookServices = require("../services/BookServices");

const getAllBooks = async (req, res) => {
  const books = await BookServices.getAllBooks();
  return res.send(books);
};

const getBook = async (req, res) => {
  const id = req.params.id;

  const book = await BookServices.getBookById(id);

  if (!book) return res.sendStatus(400);

  console.log("🚀 ~ file: book.js ~ line 16 ~ book", book);

  return res.send(book);
};

const createBook = async (req, res, next) => {
  try {
    if (!req.body) return res.sendStatus(400);

    const book = await BookServices.createBook(req.body);

    if (!book) return res.sendStatus(500);

    return res.status(200).send(book);
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

const updateBookById = async (req, res, next) => {
  try {
    const id = req.params.id;
    const updateBookById = await BookServices.updateBook(id, req.body);

    if (!updateBookById) return res.sendStatus(401);

    return res.status(200).send(updateBookById);
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

const destroyBookId = async (req, res) => {
  try {
    const id = req.params.id;
    const deleteBook = await BookServices.destroyBook(id);

    if (!deleteBook) return res.sendStatus(404);
    return res.status(200).send(deleteBook);
  } catch (error) {
    console.log(error);
    return res.sendStatus(500);
  }
};

module.exports = {
  getAllBooks,
  getBook,
  createBook,
  updateBookById,
  destroyBookId,
};
