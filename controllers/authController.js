const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const AuthService = require("../services/AuthServices");

const registerAccount = async (req, res) => {
  try {
    const { email, password, fullName } = req.body;

    const salt = await bcrypt.genSalt(process.env.SALT_ROUNDS);
    const hashPassword = await bcrypt.hashSync(password, salt);

    const newUser = {
      email: email,
      password: hashPassword,
      fullName: fullName,
    };

    const register = await AuthService.register(newUser);
    return res.send(register);
  } catch (error) {
    console.log("register controllers: ", error);
  }
};

const login = async (req, res) => {
  try {
    const { email, password } = req.body;

    const handleFindUser = await AuthService.login(email);

    if (!handleFindUser) return res.sendStatus(404);

    const isPasswordValid = await bcrypt.compare(
      password,
      handleFindUser.password
    );

    if (!isPasswordValid) return res.sendStatus(404);

    if (handleFindUser && isPasswordValid) {
      const accessToken = jwt.sign(
        {
          userId: handleFindUser._id,
          email: handleFindUser.email,
          fullName: handleFindUser.fullName,
        },
        process.env.SECRET_ACCESS_KEY,
        {
          expiresIn: "1h",
        }
      );

      res.cookie("Access_Token: ", accessToken, {
        http: true,
        sameSite: "strict",
      });
    }

    return res.send(`Login successfully ${handleFindUser}`);
  } catch (error) {
    console.log("login controllers: ", error);
  }
};

module.exports = { registerAccount, login };
