const AuthRepository = require("../repository/AuthRepository");

const register = async (data) => {
  try {
    const handleRegister = await AuthRepository.register(data);
    return handleRegister;
  } catch (error) {
    console.log(error);
  }
};

const login = async (data) => {
  try {
    const handleLogin = await AuthRepository.login(data);
    return handleLogin;
  } catch (error) {
    console.log(error);
  }
};

module.exports = { register, login };
