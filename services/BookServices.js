const BookRepository = require("../repository/BookRepository");

const getAllBooks = async () => {
  try {
    const books = await BookRepository.findAllBooks();
    return books;
  } catch (error) {
    console.log(
      "🚀 ~ file: BookServices.js ~ line 25 ~ getAllBooks ~ error",
      error
    );
  }
};

const getBookById = async (id) => {
  try {
    const book = await BookRepository.findBookById(id);
    return book;
  } catch (error) {
    console.log(
      "🚀 ~ file: BookServices.js ~ line 33 ~ getBookById ~ error",
      error
    );
  }
};

const createBook = async (data) => {
  try {
    const book = await BookRepository.createBook(data);
    return book;
  } catch (error) {
    console.log(error);
  }
};

const updateBook = async (id, data) => {
  try {
    const update = await BookRepository.updateBook(id, data);
    return update;
  } catch (error) {
    console.log(error);
  }
};

const destroyBook = async (id) => {
  try {
    const deleteBook = await BookRepository.destroy(id);
    return deleteBook;
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  getAllBooks,
  getBookById,
  createBook,
  destroyBook,
  updateBook,
};
