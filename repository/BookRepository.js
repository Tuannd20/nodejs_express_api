const BookModel = require("../database/models/Book");

const findAllBooks = async () => {
  try {
    const books = await BookModel.find({});
    return books;
  } catch (error) {
    console.log(
      "🚀 ~ file: BookRepository.js ~ line 25 ~ findAll ~ error",
      error
    );
  }
};

const findBookById = async (id) => {
  try {
    const book = await BookModel.findById(id);
    return book;
  } catch (error) {
    console.log(
      "🚀 ~ file: BookRepository.js ~ line 33 ~ findBookById ~ error",
      error
    );
  }
};

const createBook = async (data) => {
  try {
    const book = await BookModel.create(data);
    return book;
  } catch (error) {
    console.log("Create failed ", error);
  }
};

const updateBook = async (id, data) => {
  try {
    const updateBook = await BookModel.findByIdAndUpdate(id, data);
    console.log("Data to update: ", updateBook);
    return updateBook;
  } catch (error) {
    console.log(error);
  }
};

const destroy = async (id) => {
  try {
    const deleteBook = await BookModel.findByIdAndDelete(id);
    return deleteBook;
  } catch (error) {
    console.log("Delete failed ", error);
  }
};

module.exports = {
  findAllBooks,
  findBookById,
  createBook,
  destroy,
  updateBook,
};
