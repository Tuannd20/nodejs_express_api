const BookRepository = require("./BookRepository");
const DB = require("../database/connect");

beforeAll(async () => {
  await DB.connectDatabase();
});

afterAll(async () => {
  await DB.disconnectDatabase();
});

const mockBook = {
  _id: "62afd6301880d15172727cc8",
  title: "Ngay mai la tuong lai",
  price: 30000,
  author: "Nodejs",
};

describe("Sunny case: Book testing", () => {
  it("getAllBooks work OK", async () => {
    const books = await BookRepository.findAllBooks();
    console.log(
      "🚀 ~ file: BookRepository.test.js ~ line 15 ~ it ~ books",
      books
    );
    expect(books).toBeTruthy();
    expect(typeof books).toBe("object");
    expect(books.length).toBeGreaterThan(0);
  });

  it("findBookById work OK", async () => {
    const book = await BookRepository.findBookById("62afd6301880d15172727cc8");
    console.log(
      "🚀 ~ file: BookRepository.test.js ~ line 21 ~ it ~ book",
      book
    );

    expect(book).toBeTruthy();
    expect(typeof book).toBe("object");
    expect(book.title).toBe(mockBook.title);
  });

  it("create book is successful", async () => {
    const book = await BookRepository.createBook({
      title: "New book",
      price: 200,
    });
    console.log(book);
  });

  it("Update book is successful", async () => {
    const updateBook = await BookRepository.updateBook(
      "62afd6301880d15172727cc8"
    );

    expect(updateBook.title).toBe(mockBook.title);
    console.log(updateBook);
  });
});

describe("Rainy case: Book testing", () => {});
