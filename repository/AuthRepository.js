const UserModel = require("../database/models/User");

const register = async (data) => {
  try {
    const register = await UserModel.create(data);
    return register;
  } catch (error) {
    console.log("register: ", error);
  }
};

const login = async (data) => {
  try {
    const login = await UserModel.findOne({ email: data });
    return login;
  } catch (error) {
    console.log("login repository: ", error);
  }
};

module.exports = { register, login };
