var express = require("express");
var router = express.Router();
const BookController = require("../../controllers/bookController");

/**
 * @swagger
 * /api/books:
 *   get:
 *     tags:
 *       - Book
 *     summary: Find all books.
 *     description: Retrieve a list of users from JSONPlaceholder. Can be used to populate a list of fake users when prototyping or testing an API.
 *     responses:
 *       200:
 *         description: A list of books.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                type: object
 *                properties:
 *                  _id:
 *                    type: string
 *                  title:
 *                    type: string
 *                  price:
 *                    type: number
 */

router.get("/books", BookController.getAllBooks);

/**
 * @swagger
 * /api/book/{bookId}:
 *   get:
 *     tags:
 *       - Book
 *     summary: Find book by id.
 *     parameters:
 *      - in: path
 *        name: bookId
 *        required: true
 *        schema:
 *         type: string
 *     responses:
 *       200:
 *         description: Find book by id.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                type: object
 *                properties:
 *                  _id:
 *                    type: string
 *                  title:
 *                    type: string
 *                  author:
 *                    type: string
 *                  price:
 *                    type: number
 */

router.get("/book/:id", BookController.getBook);

/**
 * @swagger
 * /api/books:
 *   post:
 *     tags:
 *       - Book
 *     summary: Create a new book
 *     requestBody:
 *      content:
 *          application/json:
 *             schema:
 *               type: array
 *               items:
 *                type: object
 *                properties:
 *                  title:
 *                    type: string
 *                  price:
 *                    type: number
 *                  author:
 *                    type: string
 *     responses:
 *       201:
 *         description: Created.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                type: object
 *                properties:
 *                  _id:
 *                    type: string
 *                  title:
 *                    type: string
 *                  price:
 *                    type: number
 *                  author:
 *                    type: string
 */
router.post("/books", BookController.createBook);

/**
 * @swagger
 * /api/book/{bookId}:
 *   put:
 *     tags:
 *       - Book
 *     summary: Update a book
 *     parameters:
 *      - in: path
 *        name: bookId
 *        required: true
 *        schema:
 *         type: string
 *         description: string id of user to delete
 *     requestBody:
 *      content:
 *          application/json:
 *             schema:
 *                type: object
 *                properties:
 *                  title:
 *                    type: string
 *                  price:
 *                    type: number
 *                  author:
 *                    type: string
 *     responses:
 *       200:
 *         description: Book that was update.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                  _id:
 *                    type: string
 *                  title:
 *                    type: string
 *                  author:
 *                    type: string
 *                  price:
 *                    type: number
 */
router.put("/book/:id", BookController.updateBookById);

/**
 * @swagger
 * /api/book/{bookId}:
 *   delete:
 *     tags:
 *       - Book
 *     summary: Delete a book
 *     parameters:
 *      - in: path
 *        name: bookId
 *        required: true
 *        schema:
 *         type: string
 *         description: string id of user to delete
 *     responses:
 *       200:
 *         description: Book that was deleted.
 */
router.delete("/book/:id", BookController.destroyBookId);

module.exports = router;
