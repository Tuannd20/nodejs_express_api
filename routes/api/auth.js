var express = require("express");
var router = express.Router();

const AuthController = require("../../controllers/authController");

router.post("/register", AuthController.registerAccount);

router.post("/login", AuthController.login);

module.exports = router;
