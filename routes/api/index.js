var express = require("express");
let apiRouter = express.Router();

const bookRouter = require("./book");
const userRouter = require("./user");
const authRouter = require("./auth");

apiRouter.use("/", bookRouter);
apiRouter.use("/", userRouter);
apiRouter.use("/auth", authRouter);

module.exports = apiRouter;
